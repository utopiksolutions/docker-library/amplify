FROM node:16-slim

LABEL maintainer="Utopik Solutions"

RUN apt-get update && apt-get install -y \
  openssh-client \
  curl \
  git \
  && rm -rf /var/lib/apt/lists/*

# Install Cypress
ENV VERSION_CYPRESS=9.3
RUN npm install -g cypress@${VERSION_CYPRESS}

# Install Amplify
ENV VERSION_AMPLIFY=7.6.23
RUN npm install -g @aws-amplify/cli@${VERSION_AMPLIFY}

ENTRYPOINT [ "bash", "-c" ]
